class Professor < Person
  attr_accessor :name, :cpf, :age, :adress, :birth, :formation, :subject
  def initialize (name, cpf, age, adress, birth)

    super name, cpf, age, adress, birth 
    @formation = ['Ciencia da Computacao','Licenciatura em Fisica']
    @subject =  ['Matematica','Fisica','Quimica']
  end
  def presentation
	yield @name, @age, @birth, @adress, @cpf, @formation, @subject
  end
end
