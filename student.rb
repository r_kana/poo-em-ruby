class Student < Person
	attr_accessor :matricula, :studying, :name, :cpf, :age, :adress, :birth
	def initialize (name, cpf, age, adress, birth, matricula)
		super name, cpf, age, adress, birth 
		@matricula = matricula
		@studying = false
	end
	def moving 
		@studying = !@studying
	end
	def studying? 
		if @studying
			return true
    else
      return false
		end
	end
	def presentation
		yield @name, @age, @birth, @adress, @cpf, @matricula
	end
	def activite
    if studying? 
		  puts "Atividade: Estudando\n"
    else
      puts "Atividade: Ver tv\n"
    end
  end
end

		

