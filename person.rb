class Person
	
	attr_accessor :name, :cpf, :age, :adress, :birth, :humor_dia
	def initialize (name, cpf, age, adress, birth)
		@humor_dia = {segunda: 'Cansado', terça: 'Emburrado', quarta: 'Tranquilo', quinta: 'Irritado', sexta: 'Feliz'}
		@name = name
		@cpf = cpf
		@age = age
		@adress = adress
		@birth = birth
	end
	def presentation
		yield @name, @age, @birth, @adress, @cpf
		
	end
	def humor dia
		puts "Humor: #{@humor_dia[dia]}\n"
	end
	def activite
		puts "Atividade: Ver tv\n"
	end
end
