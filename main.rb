require_relative 'person.rb'
require_relative 'student.rb'
require_relative 'professor.rb'

p = Person.new('Fulano', '115406', '25', 'Rua sem saida', '23-11-1990')
p.presentation do |name, age, birth, adress, cpf|
	puts "Nome: #{name}\nIdade: #{age}\nNascido em: #{birth}\nEndereço: #{adress}\nCPF: #{cpf}"
end
p.humor :segunda
p.activite

s = Student.new('Fulano', '115406', '25', 'Rua sem saida', '23-11-1990', '217031')
s.presentation do |name, age, birth, adress, cpf, matricula|
	puts "Nome: #{name}\nIdade: #{age}\nNascido em: #{birth}\nEndereço: #{adress}\nCPF: #{cpf}\nMatricula: #{matricula}"
end
s.moving
s.activite
f = Professor.new('Ciclano','123456', '30', 'Rua com duas saidas', '11-08-1980')
f.presentation do |name, age, birth, adress, cpf, formation, subject|
	puts "Nome: #{name}\nIdade: #{age}\nNascido em: #{birth}\nEndereço: #{adress}\nCPF: #{cpf}\nFormacao: #{formation[0]}\nMateria: #{subject[0]}\n"
end